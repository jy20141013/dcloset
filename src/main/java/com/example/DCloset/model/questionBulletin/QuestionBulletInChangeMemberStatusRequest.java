package com.example.DCloset.model.questionBulletin;

import com.example.DCloset.enums.QuestionStatus;
import lombok.Getter;
import lombok.Setter;

// Member Id로 게시판 상태 변경
@Getter
@Setter

public class QuestionBulletInChangeMemberStatusRequest {

    private QuestionStatus questionStatus;
}
