package com.example.DCloset.model.delivery;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class DeliveryItem {

    private Long id;
    private Long ordersId;
    private String deliveryType;
    private LocalDate deliveryDate;
    private String deliveryNumber;
}
