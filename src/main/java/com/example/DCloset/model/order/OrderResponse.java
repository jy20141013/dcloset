package com.example.DCloset.model.order;
import com.example.DCloset.entity.Goods;
import com.example.DCloset.entity.Member;
import com.example.DCloset.enums.GoodsSize;
import com.example.DCloset.enums.OrderStatus;
import com.example.DCloset.enums.RefundAmount;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class OrderResponse {
    private Long id;
    private Member member;
    private Goods goods;
    private String rentalType;
    private GoodsSize goodsSize;
    private String goodsColor;
    private LocalDate orderDate;
    private LocalDate desiredDate;
    private LocalDate deadlineDate;
    private OrderStatus orderStatus;

}
