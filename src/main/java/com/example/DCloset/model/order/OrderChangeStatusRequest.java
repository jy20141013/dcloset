package com.example.DCloset.model.order;

import com.example.DCloset.enums.OrderStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class OrderChangeStatusRequest {

    private OrderStatus  orderStatus;

}
