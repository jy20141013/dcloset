package com.example.DCloset.model.charge;

import com.example.DCloset.enums.PayStatus;
import com.example.DCloset.enums.PayWay;
import com.example.DCloset.enums.PaymentAmount;
import com.example.DCloset.enums.RefundAmount;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChargeCreateRequest {

    private PayStatus payStatus;
    private String rentalType;
    private PayWay payWay;
    private String payInfo;

}
