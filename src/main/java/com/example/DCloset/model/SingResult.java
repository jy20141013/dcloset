package com.example.DCloset.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SingResult<T> extends CommonResult {
    private T data;
}
