package com.example.DCloset.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberDupNYPersonalResponse {
    private Boolean NYPersonalInfo;
}
