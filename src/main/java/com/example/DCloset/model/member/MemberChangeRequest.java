package com.example.DCloset.model.member;

import com.example.DCloset.enums.MemberGrade;
import com.example.DCloset.enums.PayDay;
import com.example.DCloset.enums.PayWay;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberChangeRequest {
    private String memberName;
    private String password;
    private String passwordRe;
    private String phoneNumber;
    private String memberAddress;
    private String memberDetailedAddress;
    private Integer postCode;
    private Boolean NYMarketing;
    private Integer remainTime;
    private PayWay payWay;
    private String payInfo;
    private PayDay payDay;
    private MemberGrade memberGrade;
}
