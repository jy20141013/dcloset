package com.example.DCloset.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonResult {
    private Integer code;
    private String msg;
}
