package com.example.DCloset.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RefundAmount {

    MEMBERSHIP("멤버십 대여", new Double[] {0D, 30000D, 60000D, 90000D} ),
    DAILY("하루 대여", new Double[] {39900D, 39900D, 39900D, 39900D}),
    OFFLINE("오프라인 대여", new Double[] {30000D, 30000D, 30000D,30000D}),
    EXPERIENCE("무료 대여", new Double[] {0D, 0D, 0D, 0D}),
    NONE("환불 금액 없음", new Double[]{0D, 0D, 0D, 0D});

    private final String paymentType;
    private final Double[] rentalPrice;
}
