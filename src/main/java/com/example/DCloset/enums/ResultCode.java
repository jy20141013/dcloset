package com.example.DCloset.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCES(0, "성공하였습니다."),
    FAILED(-1, "실패하였습니다."),
    MISSING_DATA(-10000, "데이터를 찾을 수 없습니다."),
    DUPLICATE_ID(-10001, "중복된 아이디입니다."),
    WRONG_PASSWORD(-10002, "비밀번호와 비밀번호 확인이 일치하지 않습니다."),
    IMPOSSIBLE_MEMBERSHIP_ORDER(-10010, "멤버십 대여가 불가능한 상품입니다."),
    IMPOSSIBLE_DAILY_ORDER(-10011, "하루 대여가 불가능한 상품입니다."),
    IMPOSSIBLE_EXPERIENCE_ORDER(-10012, "무료 대여가 불가능한 상품입니다."),
    IMPOSSIBLE_OFFLINE_ORDER(-10013, "오프라인 대여가 불가능한 상품입니다."),
    FAILED_DESIRED_DATE(-10014, "오늘 날짜 이전으로 주문할 수 없습니다.");

    private final Integer code;
    private final String msg;
}
