package com.example.DCloset.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PaymentAmount {
    MEMBERSHIP("멤버십 대여", 99000D),
    DAILY("하루 대여", 39900D),
    OFFLINE("오프라인 대여", 30000D),
    EXPERIENCE("무료 대여", 0D);

    private final String paymentType;
    private final Double paymentPrice;
}
