package com.example.DCloset.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter

public enum QuestionStatus {

RECEIVING("접수"),
PROCESSING("처리중"),
ANSWER_OK("답변완료")
;

private final String QuestionStatus;

}
