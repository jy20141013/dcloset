package com.example.DCloset.controller;

import com.example.DCloset.entity.Member;
import com.example.DCloset.model.CommonResult;
import com.example.DCloset.model.ListResult;
import com.example.DCloset.model.SingResult;
import com.example.DCloset.model.noticeBulletin.NoticeBulletinChangeRequest;
import com.example.DCloset.model.noticeBulletin.NoticeBulletinCreateRequest;
import com.example.DCloset.model.noticeBulletin.NoticeBulletinItem;
import com.example.DCloset.model.noticeBulletin.NoticeBulletinResponse;
import com.example.DCloset.service.MemberService;
import com.example.DCloset.service.NoticeBulletinService;
import com.example.DCloset.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/NoticeBulletin")
@CrossOrigin(origins = "*")

public class NoticeBulletinController {

    private final NoticeBulletinService noticeBulletinService;
    private final MemberService memberService;

    //등록 C -
    @PostMapping("/new/member-id/{memberId}")
    public CommonResult setNotice(@PathVariable long memberId, @RequestBody NoticeBulletinCreateRequest request){
        Member member = memberService.getMember(memberId);
        noticeBulletinService.setNotice(member,request);
        return ResponseService.getSuccessResult();
    }
    //공지사항 리스트 불러오기
    @GetMapping("/list/all")
    public ListResult<NoticeBulletinItem> getNotices (){

        return ResponseService.getListResult(noticeBulletinService.getNotices());
    }
    //공지사항 상세확인
    @GetMapping("/detail/{id}")
    public SingResult getNotice(@PathVariable long id) {
        return ResponseService.getSingResult(noticeBulletinService.getNotice(id));
    }

    //공지사항 수정하기
    @PutMapping("//{id}")
    public CommonResult NoticeBulletinChangeRequest (@PathVariable long id,@RequestBody NoticeBulletinChangeRequest request){
        noticeBulletinService.putNotice(id, request);

        return ResponseService.getSuccessResult();
    }

    //공지사항 삭제하기
    @DeleteMapping("/delete/{id}")
    public CommonResult delNotice (long id) {
        noticeBulletinService.delNotice(id);
        return ResponseService.getSuccessResult();
    }



}
