package com.example.DCloset.controller;

import com.example.DCloset.model.CommonResult;
import com.example.DCloset.model.ListResult;
import com.example.DCloset.model.SingResult;
import com.example.DCloset.model.member.*;

import com.example.DCloset.service.MemberService;
import com.example.DCloset.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
@CrossOrigin(origins = "*")
public class MemberController {
    private final MemberService memberService;

    // 회원 가입 시 아이디 중복 검사 기능
    @GetMapping("/username/check")
    public SingResult getMemberIdDubCheck(@RequestParam(name = "username") String username) {
        return ResponseService.getSingResult(memberService.getMemberIdDubCheck(username));
    }

    @GetMapping("/this-month/join")
    public SingResult<Object> thisMonthJoinCount() {
        return ResponseService.getSingResult(memberService.thisMonthJoinCount());
    }

    // 회원 가입 기능
    @PostMapping("/new")
    public CommonResult setMember (@RequestBody MemberJoinRequest request) throws Exception {
        memberService.setMember(request);
        return ResponseService.getSuccessResult();
    }

    // 전체 회원 리스트 조회
    @GetMapping("/all")
    public ListResult<MemberItem> getMembers() {
        return ResponseService.getListResult(memberService.getMembers());
    }

    // 멤버십 가입 기능
    @PutMapping("/membership/member-id/{memberId}")
    public CommonResult joinMembership(@PathVariable long memberId, @RequestBody Membership membership) {
        memberService.joinMembership(memberId, membership);

        return ResponseService.getSuccessResult();
    }

    // 회원 정보 수정
    @PutMapping("/member/change/{id}")
    public CommonResult putMemberChangeInformation(@PathVariable long id, @RequestBody MemberChangeRequest request){

        memberService.putMemberChangeInformation(id, request);

        return ResponseService.getSuccessResult();
    }

    // 멤버십 탈퇴 기능
    @PutMapping("/withdrawal-membership/member-id/{memberId}")
    public CommonResult withDrawalMembership(@PathVariable long memberId) {
        memberService.withDrawalMembership(memberId);

        return ResponseService.getSuccessResult();
    }


    // 회원 탈퇴 신청 ( 회원 등급 비회원으로 변경 )
    @PutMapping("/member-withdrawal/member-id/{memberId}")
    public CommonResult memberWithdrawal(@PathVariable long memberId) {
        memberService.memberWithdrawal(memberId);

        return ResponseService.getSuccessResult();
    }

    // 회원 상세 조회
    @GetMapping("/detail/{id}")
    public SingResult getResponse(@PathVariable long id) {
        return ResponseService.getSingResult(memberService.getMember(id));
    }
}
