package com.example.DCloset.controller;

import com.example.DCloset.entity.Member;
import com.example.DCloset.model.CommonResult;
import com.example.DCloset.model.ListResult;
import com.example.DCloset.model.SingResult;
import com.example.DCloset.model.charge.ChargeCreateRequest;
import com.example.DCloset.model.charge.ChargeInfoItem;
import com.example.DCloset.model.charge.ChargeResponse;
import com.example.DCloset.service.ChargeService;
import com.example.DCloset.service.MemberService;
import com.example.DCloset.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/charge")
@CrossOrigin(origins = "*")
public class ChargeController {

    private final ChargeService chargeService;
    private final MemberService memberService;

    //회원아이디를 받아서 결제 등록
    @PostMapping("/new/member-id/{memberId}")
    public CommonResult setCharge(@PathVariable long memberId, @RequestBody ChargeCreateRequest request){
        Member member = memberService.getMember(memberId);
        chargeService.setCharge(member,request);
        return ResponseService.getSuccessResult();

    }
    @GetMapping("/all")
    public ListResult<ChargeInfoItem> getList(){
       return ResponseService.getListResult(chargeService.getChargeList());

    }
    @GetMapping("/detail/{id}")
    public SingResult getCharge(@PathVariable long id){
        return ResponseService.getSingResult(chargeService.getCharge(id));

    }
    @GetMapping("detail/member-id/{memberId}")
    public SingResult getChargeByMember(@PathVariable long memberId) {
        Member member = memberService.getMember(memberId);
        return ResponseService.getSingResult(chargeService.getChargeByMember(member));
    }

    @PutMapping("refund/member-id/{memberId}")
    public CommonResult putChargeChangeRequest(@PathVariable long memberId){
        Member member = memberService.getMember(memberId);
        chargeService.putChargeChangeRequest(member);
        return ResponseService.getSuccessResult();

    }


}
