package com.example.DCloset.controller;


import com.example.DCloset.model.CommonResult;
import com.example.DCloset.model.ListResult;
import com.example.DCloset.model.SingResult;
import com.example.DCloset.model.goods.GoodsChangeRequest;
import com.example.DCloset.model.goods.GoodsCreateRequest;
import com.example.DCloset.model.goods.GoodsItem;
import com.example.DCloset.service.GoodsService;
import com.example.DCloset.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/goods")
@CrossOrigin(origins = "*")
public class GoodsController {

    private final GoodsService goodsService;


    //상품 정보 등록하기
    @PostMapping("/new")
    public CommonResult setGoods(@RequestBody GoodsCreateRequest request) {
        goodsService.setGoods(request);
        return ResponseService.getSuccessResult();
    }

    //상품 전체 리스트 불러오기
    @GetMapping("/all")
    public ListResult<GoodsItem> getGoodsList (){

        return ResponseService.getListResult(goodsService.getGoodsList());
    }

    //상품 하나 불러오기
    @GetMapping("/detail/id/{id}")
    public SingResult getGoods(@PathVariable long id) {

        return ResponseService.getSingResult(goodsService.getGood(id));
    }


    //상품 정보 수정하기
    @PutMapping("/change/id/{id}")
    public CommonResult putChange ( @PathVariable long id,@RequestBody GoodsChangeRequest request ){

        goodsService.putChange(id,request);
        return ResponseService.getSuccessResult();
    }
}
