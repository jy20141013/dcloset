package com.example.DCloset.service;
import com.example.DCloset.entity.Delivery;
import com.example.DCloset.entity.Orders;
import com.example.DCloset.model.delivery.*;
import com.example.DCloset.repository.DeliveryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DeliveryService {

    private final DeliveryRepository deliveryRepository;


    // 등록
    public void setDelivery(Orders orders, DeliveryCreateRequest request) {

        Delivery addData = new Delivery();

        addData.setOrders(orders);
        addData.setDeliveryType(request.getDeliveryType());
        addData.setDeliveryDate(LocalDate.now());
        addData.setDeliveryNumber(request.getDeliveryNumber());

        deliveryRepository.save(addData);

    }

    // 전체 리스트
    public List<DeliveryItem> getDeliveryList() {

        List<Delivery> originList = deliveryRepository.findAll();
        List<DeliveryItem> result = new LinkedList<>();

        for (Delivery delivery : originList) {

            DeliveryItem addItem = new DeliveryItem();

            addItem.setId(delivery.getId());
            addItem.setOrdersId(delivery.getOrders().getId());
            addItem.setDeliveryType(delivery.getDeliveryType().getDeliveryType());
            addItem.setDeliveryDate(delivery.getDeliveryDate());
            addItem.setDeliveryNumber(delivery.getDeliveryNumber());

            result.add(addItem);
        }

        return result;


    }

    // id로 정보 찾기
    public DeliveryResponse getDelivery(long id){

        Delivery originData = deliveryRepository.findById(id).orElseThrow();

        DeliveryResponse response = new DeliveryResponse();

        response.setId(originData.getId());
        response.setOrdersId(originData.getOrders().getId());
        response.setDeliveryType(originData.getDeliveryType());
        response.setDeliveryDate(originData.getDeliveryDate());
        response.setDeliveryNumber(originData.getDeliveryNumber());


        return response;

    }




    //id로 배송 수정
    public void  putDeliveryChange(long id, DeliveryChangeRequest request) {

        Delivery originData = deliveryRepository.findById(id).orElseThrow();

        originData.setDeliveryDate(request.getDeliveryDate());
        originData.setDeliveryType(request.getDeliveryType());
        originData.setDeliveryNumber(request.getDeliveryNumber());

        deliveryRepository.save(originData);
    }

    public void putDeliveryOrderChange(Orders orders, DeliveryChangeOrderRequest request){
        Delivery originData = deliveryRepository.findById(orders.getId()).orElseThrow();

        originData.setDeliveryDate(request.getDeliveryDate());
        originData.setDeliveryNumber(request.getDeliveryNumber());
        originData.setDeliveryType(request.getDeliveryType());

        deliveryRepository.save(originData);
    }


}