package com.example.DCloset.service;
import com.example.DCloset.entity.Charge;
import com.example.DCloset.entity.Member;
import com.example.DCloset.enums.PayStatus;
import com.example.DCloset.enums.PaymentAmount;
import com.example.DCloset.enums.RefundAmount;
import com.example.DCloset.model.charge.ChargeCreateRequest;
import com.example.DCloset.model.charge.ChargeInfoItem;
import com.example.DCloset.model.charge.ChargeResponse;
import com.example.DCloset.repository.ChargeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ChargeService {

    private final ChargeRepository chargeRepository;

    public void setCharge(Member member, ChargeCreateRequest request){

        Charge addData = new Charge();

        addData.setMember(member);
        addData.setActualPayDay(LocalDateTime.now());
        addData.setPayStatus(PayStatus.PAYMENT);
        addData.setRentalType(request.getRentalType());
        addData.setPaymentAmount(PaymentAmount.valueOf(request.getRentalType()).getPaymentPrice());
        addData.setRefundAmount(RefundAmount.NONE.getRentalPrice()[member.getRemainTime()]);
        addData.setPayWay(request.getPayWay());
        addData.setPayInfo(request.getPayInfo());

        // 처음 결제 내역이 등록될 때는 결제 상태가 PAYMENT로 고정, 환불 금액은 ENUM 활용 0원 세팅.

        chargeRepository.save(addData);
    }

    public List<ChargeInfoItem> getChargeList(){

        List<Charge> originList = chargeRepository.findAll();
        List<ChargeInfoItem> result = new LinkedList<>();

        for(Charge charge:originList) {

            ChargeInfoItem addList = new ChargeInfoItem();

            addList.setId(charge.getId());
            addList.setActualPayDay(charge.getActualPayDay());
            addList.setPayStatus(charge.getPayStatus());
            addList.setPaymentAmount(charge.getPaymentAmount());
            addList.setRefundAmount(charge.getRefundAmount());
            addList.setPayInfo(charge.getPayInfo());
            addList.setPayWay(charge.getPayWay());
            addList.setMember(charge.getMember());
            result.add(addList);

        }
        return result;
    }

    // 결제 내역 id 단수 R
    public ChargeResponse getCharge(long id) {

        Charge originData = chargeRepository.findById(id).orElseThrow();

        ChargeResponse response = new ChargeResponse();

        response.setId(originData.getId());
        response.setActualPayDay(originData.getActualPayDay());
        response.setMember(originData.getMember());
        response.setPayStatus(originData.getPayStatus());

        response.setPaymentAmount(originData.getPaymentAmount());
        response.setRefundAmount(originData.getRefundAmount());
        response.setPayInfo(originData.getPayInfo());
        response.setPayWay(originData.getPayWay());

        return response;

    }

    // 멤버 아이디로 단수 R
    public ChargeResponse getChargeByMember(Member member) {
        Charge originData = chargeRepository.findById(member.getId()).orElseThrow();

        ChargeResponse response = new ChargeResponse();

        response.setId(originData.getId());
        response.setActualPayDay(originData.getActualPayDay());
        response.setMember(originData.getMember());
        response.setPayStatus(originData.getPayStatus());

        response.setPaymentAmount(originData.getPaymentAmount());
        response.setRefundAmount(originData.getRefundAmount());
        response.setPayInfo(originData.getPayInfo());
        response.setPayWay(originData.getPayWay());

        return response;
    }


    // 환불 신청
    public void putChargeChangeRequest(Member member){

        Charge charge = chargeRepository.findById(member.getId()).orElseThrow();

        charge.setRefundAmount(RefundAmount.valueOf(charge.getRentalType()).getRentalPrice()[member.getRemainTime()]);
        charge.setPayStatus(PayStatus.REFUND);
        charge.setRefundDay(LocalDateTime.now());

        chargeRepository.save(charge);
        
    }
}
