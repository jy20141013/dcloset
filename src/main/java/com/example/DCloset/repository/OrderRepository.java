package com.example.DCloset.repository;

import com.example.DCloset.entity.Orders;
import com.example.DCloset.enums.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Orders, Long> {
    long countByOrderStatus(OrderStatus orderStatus);
}
