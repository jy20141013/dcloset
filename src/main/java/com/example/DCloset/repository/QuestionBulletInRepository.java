package com.example.DCloset.repository;

import com.example.DCloset.entity.QuestionBulletin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionBulletInRepository extends JpaRepository <QuestionBulletin,Long> {
}
